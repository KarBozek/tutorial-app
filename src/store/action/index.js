import {actions} from '../reducer/auth';

export const {clearAuth, setUserData} = actions;
