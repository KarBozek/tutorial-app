import React from 'react';
import Layout from '../components/Layout/Layout';

const withLayoutWrapper = Component => {
    const withLayout = props => (
        <Layout>
            <Component {...props} />
        </Layout>
    );
    return withLayout;
};

export default withLayoutWrapper;
