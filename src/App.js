import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import NotificationProvider from './context/NotificationContext';
import * as routes from './constants/routes';
import './App.css';

import HomePage from './pages/Home/Home';

const store = configureStore();

const app = () => (
    <Provider store={store}>
        <NotificationProvider>
            <BrowserRouter>
                <Routes>
                    <Route path={routes.HOME} element={<HomePage />} />
                    <Route path={routes.LOGIN} element={<HomePage />} />
                    {/*<Route path="*" element={<NotFound />} />*/}
                </Routes>
            </BrowserRouter>
        </NotificationProvider>
    </Provider>
);

export default app;
