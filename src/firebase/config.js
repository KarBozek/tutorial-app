import {initializeApp} from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
    apiKey: 'AIzaSyC6RddrcCABhOxoaw9rNVlHOatCfy3KDUI',
    authDomain: 'tutorial-app-16432.firebaseapp.com',
    projectId: 'tutorial-app-16432',
    storageBucket: 'tutorial-app-16432.appspot.com',
    messagingSenderId: '944594625204',
    appId: '1:944594625204:web:5daa84096d7d5000186de1',
    measurementId: 'G-Q0F4223HSJ',
};

const firebase = initializeApp(config);
export default firebase;
