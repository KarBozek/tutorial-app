import React, {createContext, useState} from 'react';
import styles from './NotificationContext.module.css';
import clsx from 'clsx';
// import {CloseIcon, SuccessIcon} from '../assets';

export const CustomToastContext = createContext({
    notifications: [],
    addNotification: () => {},
    removeNotification: () => {},
});

const types = {
    success: <div />,
    error: <div />,
};

const customToast = ({children}) => {
    const [state, setState] = useState({notifications: []});

    const removeNotification = id => {
        const notificationsArray = [...state.notifications];
        notificationsArray.splice(id, 1);
        setState({notifications: notificationsArray});
    };

    const addNotification = (message, type) => {
        setState({
            notifications: [...state.notifications, {message, type}],
        });
        setTimeout(
            () => removeNotification(state.notifications.lastIndexOf()),
            3000,
        );
    };

    const notificationsContext = () => ({
        state,
        addNotification: ({message, type}) => addNotification(message, type),
        removeNotification,
    });

    return (
        <CustomToastContext.Provider value={notificationsContext()}>
            {children}
            {state.notifications.map((notification, index) => (
                <div
                    key={index}
                    className={clsx(styles.alert, {
                        [styles.success]: notification.type === 'success',
                        [styles.error]: notification.type !== 'success',
                    })}
                >
                    <div className={styles.container}>
                        {types[notification.type]}
                        {notification.message}
                    </div>
                </div>
            ))}
        </CustomToastContext.Provider>
    );
};

export default customToast;
